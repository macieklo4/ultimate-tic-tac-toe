from exoh import*
import pytest
import numpy as np
from random import randint
from unittest import mock
import sys

def test_winner_for_combination():
    result1 = winner_for_combination(0,1,2)
    assert result1 == 0
    result2 = winner_for_combination(2,0,0)
    assert result2 == 0
    result3 = winner_for_combination(1,1,1)
    assert result3 == 1
    result4= winner_for_combination(2,2,2)
    assert result4 == 2

def test_winner_small():
    board0 = np.zeros((3,3))
    board1 = np.zeros((3,3))
    board2 = np.zeros((3,3))
    board3 = np.zeros((3,3))
    for i in range(3):
        board1[i,1] = 1
        board2[1,i] = 2
        board3[i,i] = 1
    assert winner_small(board0) == 0    
    assert winner_small(board1) == 1
    assert winner_small(board2) == 2  
    assert winner_small(board3) == 1

def test_convert_number_To_sign():
    assert convert_number_To_sign(0) == '- '
    assert convert_number_To_sign(1) == 'x '
    assert convert_number_To_sign(2) == 'o '

def test_full_square():
    board1 = np.zeros((3,3))
    board2 = np.ones((3,3))
    assert full_square(board1) == False
    assert full_square(board2) == True
    for i in range(3):
        for j in range(3):
            sign = random.randint(1,2)
            board2[i][j] = sign
    assert full_square(board2) == True

def test_random_place():
    coords = random_place()
    assert len(coords) == 2
    assert coords[0][0] >= 0 and coords[0][0] <=2 and type(coords[0][0]) == int
    assert coords[0][1] >= 0 and coords[0][1] <=2 and type(coords[0][1]) == int
    assert coords[1][0] >= 0 and coords[1][0] <=2 and type(coords[1][0]) == int
    assert coords[1][1] >= 0 and coords[1][1] <=2 and type(coords[1][1]) == int

def test_best_move():
    s = np.zeros((3,3))
    board = np.array([[s,s,s],[s,s,s],[s,s,s]])
    board[0,0][0,0] = 1
    board[0,0][1,1] = 1
    s_previous = (0,0)
    assert best_move(board,s_previous) == ((0,0),(2,2))
    w = np.zeros((3,3))
    board = np.array([[w,w,w],[w,w,w],[w,w,w]])
    board[1,1][0,2] = 1
    board[1,1][2,0] = 1
    s_previous = (1,1)
    assert best_move(board,s_previous) == ((1,1),(1,1))

def test_RandomComputerPlayer():
    s = np.zeros((3,3))
    w = np.ones((3,3))
    board = np.array([[s,s,s],[s,w,s],[s,s,s]])    
    s_previous = (1,1)
    coords = RandomComputerPlayer(board,s_previous)  
    assert (coords[0][0],coords[0][1]) != s_previous 

def test_GeniusComputerPlayer():
    s = np.zeros((3,3))
    w = np.ones((3,3))
    board1 = np.array([[s,s,s],[s,s,s],[s,w,s]])    
    s_previous = (2,1)
    coords1 = GeniusComputerPlayer(board1,s_previous)
    assert (coords1[1][0],coords1[1][1]) == (1,1)
    board2 = np.array([[s,s,s],[s,s,s],[s,w,s]]) 
    board2[0,2][0,0] = 2
    board2[0,2][0,1] = 2
    coords2 = GeniusComputerPlayer(board2,s_previous) 
    assert coords2 == ((0,2),(0,2))

def test_valid_input():
    s = np.zeros((3,3))
    board = np.array([[s,s,s],[s,s,s],[s,s,s]])
    board[0,2][1,1] = 1
    s_previous = (1,1)
    big_table_coordinate = (1,1)
    assert valid_input(board,big_table_coordinate,s_previous) == True

@mock.patch('builtins.input', side_effect=['7','4'])
def test_get_user_input(input):
    current_player_sign = 'x'
    s = np.zeros((3,3))
    board = np.array([[s,s,s],[s,s,s],[s,s,s]])
    board[0,2][2,1] = 1
    s_previous = (2,1)
    a = get_user_input(current_player_sign,board,s_previous)
    assert a == ((2,1),(0,0))
