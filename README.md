Ultiamate Tic-Tac_Toe is a version of worldly known game. It's played on 9x9 grid instead of just 3x3 with some additional rules.
Made in python, the game is played in terminal. 
A move consists of entering row and column index (starting with 1) of grid on which a player wants to place their symbol ('x' or 'o').
Program enables the user to choose their oponent - random or intelligent computer player.

![Screenshot_from_2022-05-20_12-31-07](/uploads/de167e95eff980c8fa4971b0b6a3a414/Screenshot_from_2022-05-20_12-31-07.png)
