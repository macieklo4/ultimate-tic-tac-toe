import numpy as np
import random
import time


def winner_for_combination(a, b, c):
    # 0 means empty square
    if a == 0 or b == 0 or c == 0:
        return 0
    elif a == b == c:
        return a
    else:
        return 0


def winner_small(small_board):
    # columns
    for i in range(0, 3):
        vertical_winner = winner_for_combination(*small_board[:, i])
        if vertical_winner:
            return vertical_winner

    # rows
    for i in range(0, 3):
        horizontal_winner = winner_for_combination(*small_board[i, :])
        if horizontal_winner:
            return horizontal_winner

    # diagonals
    diagonal1 = winner_for_combination(
        small_board[0, 0], small_board[1, 1], small_board[2, 2])
    diagonal2 = winner_for_combination(
        small_board[2, 0], small_board[1, 1], small_board[0, 2])
    return diagonal1 or diagonal2


def convert_number_To_sign(liczba):
    # if 1 then x is winner
    # if 2 then o is winner
    # if 0 then no winner

    if liczba == 0:
        return '- '
    elif liczba == 1:
        return 'x '
    elif liczba == 2:
        return 'o '


def printBoard(board, b_previous, s_previous, i):
    q = ''
    for k in range(9):
        for j in range(9):
            q += convert_number_To_sign(board[k//3, j//3][k % 3, j % 3])
            if j % 3 == 2:
                q += '   '
        q += '\n'
        if k % 3 == 2:
            q += '\n'
        a = b_previous[0]*3 + s_previous[0] + 1
        b = b_previous[1]*3 + s_previous[1] + 1
    if i % 2 == 1:
        text = f'Computers previous move was {a},{b}'
    elif i % 2 == 0:
        text = f'Your previous move was {a},{b}'
    print('\n')
    print(text)
    print('\n')
    print(q)


def full_square(small_board):
    for i in range(3):
        for j in range(3):
            if small_board[i][j] == 0:
                return False
    return True


def valid_input(board, big_table_coordinate, s_previous):
    if s_previous == None:
        return True
    # forbidden move to already won small_table
    elif winner_small(board[big_table_coordinate]) != 0:
        return False
    # permitted move while full small_table on condition new small_table is not already won
    elif full_square(board[s_previous]) == True and winner_small(board[big_table_coordinate]) == 0:
        return True
    # permitted move while board is already won
    elif winner_small(board[s_previous]) != 0 and winner_small(board[big_table_coordinate]) == 0:
        return True
    elif big_table_coordinate == s_previous:  # basic rule
        return True
    else:
        return False


def get_user_input(current_player_sign, board, s_previous):
    while True:
        try:
            a = int(
                input(f'Enter first coordinate for player {current_player_sign}: '))-1
            b = int(
                input(f'Enter second coordinate for player {current_player_sign}: '))-1
        except ValueError:
            print("Invalid input, try again")
            continue
        big_table_coordinate = (a//3, b//3)
        small_table_coordinate = (a % 3, b % 3)
        if a >= 0 and a <= 8 and b >= 0 and b <= 8 and board[big_table_coordinate][small_table_coordinate] == 0 and valid_input(board, big_table_coordinate, s_previous) == True:
            return big_table_coordinate, small_table_coordinate
        else:
            print('Invalid input, try again')


def game():

    s = np.zeros((3, 3))
    # creating array of nine 3x3 arrays
    board = np.array([[s, s, s], [s, s, s], [s, s, s]])
    i = 0
    # array representing winners on small_tables
    final_board = np.zeros((3, 3))
    s_previous = None
    b_previous = None
    TypeOfEnemy = input(
        'Write RANDOM to play against RandomComputerPlayer or GENIUS to play against GeniusComputerPlayer... ')
    while TypeOfEnemy.lower() != 'random' and TypeOfEnemy.lower() != 'genius':
        TypeOfEnemy = input(
            'Write RANDOM to play against RandomComputerPlayer or GENIUS to play against GeniusComputerPlayer... ')

    while winner_small(final_board) == 0 and i < 81:
        current_player_sign = 'x' if i % 2 == 0 else 'o'
        if i % 2 == 0:
            big_table_coordinate, small_table_coordinate = get_user_input(
                current_player_sign, board, s_previous)
        else:
            if TypeOfEnemy.lower() == 'random':
                big_table_coordinate, small_table_coordinate = RandomComputerPlayer(
                    board, s_previous)
            elif TypeOfEnemy.lower() == 'genius':
                big_table_coordinate, small_table_coordinate = GeniusComputerPlayer(
                    board, s_previous)
            time.sleep(1.5)

        # overwrite
        b_previous = big_table_coordinate
        s_previous = small_table_coordinate
        board[big_table_coordinate][small_table_coordinate] = 1 if i % 2 == 0 else 2

        if final_board[big_table_coordinate] == 0:
            final_board[big_table_coordinate] = winner_small(
                board[big_table_coordinate])
        printBoard(board, b_previous, s_previous, i)
        i += 1
    w = winner_small(final_board)
    if w == 0:
        print('Draw')
    elif w == 1:
        print('Congrats X..... You won')
    elif w == 2:
        print('Congrats O..... You won')


def random_place():
    i = 0
    tab = []
    while i < 4:
        coord = random.randint(0, 2)
        tab.append(coord)
        i += 1
    return (tab[0], tab[1]), (tab[2], tab[3])


def RandomComputerPlayer(board, s_previous):
    if winner_small(board[s_previous]) != 0:
        while True:
            coords = random_place()
            if board[coords[0][0], coords[0][1]][coords[1][0], coords[1][1]] == 0 and winner_small(board[coords[0][0], coords[0][1]]) == 0:
                return coords
    else:
        while True:
            sb_1 = random.randint(0, 2)
            sb_2 = random.randint(0, 2)
            if board[s_previous][sb_1, sb_2] == 0:
                return (s_previous), (sb_1, sb_2)


def best_move(board, s_previous):
    for i in range(3):
        for j in range(3):
            if board[s_previous][i, j] == 0:
                for k in range(1, 3):
                    board[s_previous][i, j] = k
                    # checking if instant win or block in small_table is possible
                    if winner_small(board[s_previous]) == k:
                        board[s_previous][i, j] = 0
                        return (s_previous), (i, j)
                    board[s_previous][i, j] = 0
    return None


def GeniusComputerPlayer(board, s_previous):
    if winner_small(board[s_previous]) == 0 and full_square(board[s_previous]) == False:
        if best_move(board, s_previous) == None:
            while True:
                coords = random_place()
                if board[s_previous][coords[1][0], coords[1][1]] == 0:
                    return (s_previous), (coords[1][0], coords[1][1])
        else:
            return best_move(board, s_previous)
    else:
        for i in range(0, 3):
            for j in range(0, 3):
                big_coords = (i, j)
                if winner_small(board[big_coords]) == 0 and full_square(board[big_coords]) == False:
                    if best_move(board, big_coords) == None:
                        pass
                    else:
                        return best_move(board, big_coords)
        for i in range(2):
            for j in range(2):
                big_coords = (i, j)
                # checking if any center is free
                if winner_small(board[big_coords]) == 0 and full_square(board[big_coords]) == False:
                    if board[i, j][1, 1] == 0:
                        return (i, j), (1, 1)
        while True:
            coords = random_place()
            if board[coords[0][0], coords[0][1]][coords[1][0], coords[1][1]] == 0:
                return coords


game()
